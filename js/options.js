window.URL = window.URL || window.webkitURL;

window.onload = function(e) {
    chrome.storage.local.get(["base64"], function(data) {
        if (data.base64 === undefined) {
            document.getElementById("logo").src = chrome.extension.getURL('icons/logo.png');

            getBase64FromURL(chrome.extension.getURL('icons/logo.png'), function(result) {
                chrome.storage.local.set({
                    base64: result,
                    timestamp: Date.now()
                    }, function() {
                    console.log("Default image set.");
                });
            });
        }
        else {
            document.getElementById("logo").src = "data:image/png;base64," + data.base64;
        }
    }); 
};

var form = document.getElementById('upload');
if (form.attachEvent) {
    form.attachEvent("submit", handleImage);
} else {
    form.addEventListener("submit", handleImage);
}

function handleImage(e) {
    if (e.preventDefault) e.preventDefault();

    var selectedFile = document.getElementById("file-elem").files[0];

    if (selectedFile === undefined) alert("No image selected.");
    else {
        var img = document.getElementById("logo");
        var url = window.URL.createObjectURL(selectedFile);
        img.src = url;
        getBase64FromURL(url, function(result) {
            chrome.storage.local.set({
                base64: result,
                timestamp: Date.now()
                }, function() {
                alert('Image saved successfully!');
            });
        });
        img.onload = function() {
            window.URL.revokeObjectURL(this.src);
        }
    }
}

function getBase64FromURL(URL, callback) {
    var img = new Image();
    img.src = URL;
    var dataURL;
    img.onload = function () {
        var canvas = document.createElement("canvas");
        canvas.width =this.width;
        canvas.height =this.height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(this, 0, 0);

        dataURL = canvas.toDataURL("image/png");
        callback(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
    }
}