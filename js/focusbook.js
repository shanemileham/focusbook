// Focusbook.js
// 2015 Stephen Chen

var content = [
  {
    text: "riding a bike",
    img: "url"
  },
  {
    text: "doing homework",
    img: "url"
  },
  {
    text: "taking a nap",
    img: "url"
  },
  {
    text: "reading a book",
    img: "url"
  },
  {
    text: "meditating",
    img: "url"
  }
];

function createCard(imgSrc) {
  var focusContainer = $("<div>", {
      id: "focus-container"
    }),
    focusContent = $("<div>", {
      class: "focus-content"
    }).appendTo(focusContainer),
    focusTitle = $("<p>", {
      class: "focus-title",
      text: "Get your butt off Facebook."
    }).appendTo(focusContent),
    focusActivity = $("<span>", {
      text: content[Math.floor(Math.random() * content.length)].text
    }),
    focusSubtitle = $("<p>", {
      class: "focus-subtitle"
    }).append("You could be ").append(focusActivity).append(" instead!").appendTo(focusContent)
    focusIcon = $("<img>", {
      id: "logo",
      src: imgSrc,
      alt: "Focusbook"
    }).appendTo(focusContent);

    setInterval(function() {
      $("div#pagelet_home_stream").replaceWith(focusContainer);
      $("div[id^='topnews_main_stream']").replaceWith(focusContainer);
      $("div#pagelet_trending_tags_and_topics").remove();
    }, 750);
}

chrome.storage.local.get(["base64"], function(data) {
    if (data.base64 === undefined) {
        createCard(chrome.extension.getURL('icons/logo.png'));
    }
    else {
      createCard("data:image/png;base64," + data.base64);
    }
}); 