<a href="https://chrome.google.com/webstore/detail/focusbook/apaajjfjfiplngfmknjjbdhmkfednofa">
    <img src="https://raw.githubusercontent.com/SCDevy/focusbook/master/icons/logo.png" alt="Focusbook" title="Focusbook" align="right" height="70">
</a>

# Focusbook

Focusbook hides your Facebook newsfeed so you can stop getting distracted and stay productive.

[Install from Chrome Web Store](https://chrome.google.com/webstore/detail/focusbook/apaajjfjfiplngfmknjjbdhmkfednofa)